<div id="live-options">
	<div id="lobuttons" class="clearfix">
		<a href="#" id="loclose" class="button">Close</a>
		<a href="#" class="button disabled">Save &amp; Publish</a>
	</div>
	<ul id="loptions">
		<li id="preview-notice">
			<a href="#" class="lo-title"><strong>Metrolium</strong> WP Customizer<i class="icon-chevron-down"></i></a>
			<ul>
				<li>
					<?php /*<img class="theme-screenshot" src="http://www.theme.dev/wp-content/themes/metrolium/screenshot.png">*/ ?>
					<p class="lead">Metrolium has built in WP Customizer support which means that you're able to <strong>change the look and feel of your theme with a realtime live preview!</strong></p>
					<p>Say goodbye to boring over engineered admin panels where you need to keep saving and switching tabs each time you want to preview your changes, with WP Customizer it all happens right in front of your eyes and you don't have to save and publish your changes until you're completely happy either.</p>
					<p>Please note that this is a mockup WP Customizer and is only intended to show you just how powerful Metrolium's style options are, you won't be able to save any changes in this demonstration.</p>
				</li>
			</ul>
		</li>
		<?php echo $html; ?>
	</ul>
</div>
<div id="live-options-toggle" class="active"><i class="icon-cog icon-spin"></i></div>