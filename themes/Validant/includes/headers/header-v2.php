<?php
$header_options = get_option('euged_header');
$header_theme_mods = get_theme_mod('euged_header');
?>

<div id="banner" class="band">
	<div class="inner">

		<?php $logo = !empty($header_theme_mods['logo']) ? $header_theme_mods['logo'] : get_template_directory_uri() . '/assets/images/banner_logo.png';?>
		<a href="<?php echo home_url() ?>" class="logo"><img src="<?php echo $logo; ?>" alt="<?php echo get_bloginfo('description') ?>" /></a>

		<?php if( !empty( $header_options['style_2_content'] ) ) : ?>
		<div class="content" id="header-v2-content">
			<?php echo $header_options['style_2_content'] ?>
		</div>
		<?php endif ?>

	</div>
</div>

<div id="navigation" class="band">
	<div class="inner">
		<nav class="primary">
			<?php if(has_nav_menu('primary_navigation')) : ?>
				<?php wp_nav_menu(array('theme_location' => 'primary_navigation', 'container_id' => 'primary-navigation','walker' => new Arrow_Walker_Nav_Menu)); ?>
			<?php else : ?>
				<div class="placeholder">Define a 'Primary Navigation' menu</div>
			<?php endif ?>
		</nav>
	</div>
</div>